#include<iostream>
#include <chrono>
using namespace std;
void BubbleSort(int array_to_sort[],int size_array)
{
	int swapped,temp;
	for(int x=0;x<size_array-1;x++)
	{
		swapped = 0;
		for(int y=0;y<size_array-x-1;y++ )
		{
			if(array_to_sort[y]>=array_to_sort[y+1])
			{
				temp = array_to_sort[y];
				array_to_sort[y]=array_to_sort[y+1];	
				array_to_sort[y+1]=temp;
				swapped = 1;
			}

		}
		if(swapped == 0)
		{
			break;
		}
	}	
}
void print_array(int array_to_print[],int size_array)
{
	for(int j=0;j<=size_array-1;j++)
	{		
			cout<<array_to_print[j]<<"\t";
	}

}
int main()
{	
	cout<<"Hello Bubble Sort"<<endl;
    int size_of_array;
	int n;

    //ask the user for the size of an array and create the array of the specified size
    cout<<"Enter the size of an array:\n";
    cin>>size_of_array;
	//array declaration
	n = size_of_array;
    int array_of_random_numbers[size_of_array];
    //fill the array with the random numbers
    for (int i = 0; i < size_of_array; i++)
    {
        array_of_random_numbers[i] = n--;
    }
	cout<<endl;
	    
	cout<<"The List of Unsorted Array is: \n";
	print_array(array_of_random_numbers,size_of_array);
	cout<<endl;
	cout<<endl;
    //calling bubble sort function
	chrono::steady_clock::time_point start = chrono::steady_clock::now();
	BubbleSort(array_of_random_numbers,size_of_array);
	chrono::steady_clock::time_point stop = chrono::steady_clock::now();

	cout<<"Sorted Array is : "<<endl;
	cout<<endl;
	print_array(array_of_random_numbers,size_of_array);
	cout<<endl;
	cout<<endl;

	cout<<"Time For The Loop  : "
		<<chrono::duration_cast<chrono::milliseconds>(stop - start).count();
	cout<<endl;

	return 0;
}
/*
The above function always runs O(n^2) time even if the array is sorted. It can be optimized by stopping the algorithm if inner loop didn’t cause any swap.

Some Characteristics of Bubble Sort:(Optimized)
	1.Large values are always sorted first.
	2.It only takes one iteration to detect that a collection is already sorted.
	3.The best time complexity for Bubble Sort is O(n) if optimized. The average and worst time complexity is O(n²).
	4.The space complexity for Bubble Sort is O(1), because only single additional memory space is required.

*/
