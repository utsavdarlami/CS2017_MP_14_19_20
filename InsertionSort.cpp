#include<iostream>
#include <chrono>

using namespace std;

void insertion_sort(int array_of_random_numbers[], int size)
{
    int i, key, j;
    //i is similar to the size of sorted array
    // assuming the first element is already sorted
    for(i = 1; i < size ;i++)
    {
        key = array_of_random_numbers[i];
        j = i - 1;

        /*if the element at j'th position is greater than the key then
        /move that element one position ahead and decrease by 1*/
        while(j>=0 && array_of_random_numbers[j] > key)
        {
            array_of_random_numbers[j+1] = array_of_random_numbers[j];
            j = j-1;
        }
        //pass the value of the key to the array element one position ahead of the current
        //jth position
        array_of_random_numbers[j+1] = key;
    }
}

void print_array(int array_of_rn[],int sizes )
{
    for(int i = 0 ; i < sizes; i++)
    {
        cout<<array_of_rn[i]<<"\t";
    }
}

int main()
{
	cout<<"Hello Insertion Sort"<<endl;
    int size_of_array;
    int time_complexity;
    int n;
    //ask the user for the size of an array and create the array of the specified size
    cout<<"Enter the size of an array:\n";
    cin>>size_of_array;
    n = size_of_array;
	//array declaration
    int array_of_random_numbers[size_of_array];
    //filling the numbers
    for (int i = 0; i < size_of_array; i++)
    {
        array_of_random_numbers[i] = n--;
    }
    //printing the list of unsorted array
    cout<<"The List of Unsorted Array is: \n";
    print_array(array_of_random_numbers,size_of_array);
    cout<<"\n";
	cout<<endl;

    chrono::steady_clock::time_point start = chrono::steady_clock::now();
    //calling insertion_sort function
    insertion_sort(array_of_random_numbers, size_of_array);
    chrono::steady_clock::time_point stop = chrono::steady_clock::now();
	
    cout<<"The List of Array after sorting is: \n";
	print_array(array_of_random_numbers, size_of_array);
	
    cout<<endl;
	cout<<endl;
    cout<<"Time For The Loop  : "
        <<chrono::duration_cast<chrono::milliseconds>(stop - start).count();
    cout<<endl;
    return 0;
}
